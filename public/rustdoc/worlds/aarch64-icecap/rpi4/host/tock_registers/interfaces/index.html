<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="generator" content="rustdoc"><meta name="description" content="Interfaces (traits) to register types"><meta name="keywords" content="rust, rustlang, rust-lang, interfaces"><title>tock_registers::interfaces - Rust</title><link rel="stylesheet" type="text/css" href="../../normalize.css"><link rel="stylesheet" type="text/css" href="../../rustdoc.css" id="mainThemeStyle"><link rel="stylesheet" type="text/css" href="../../light.css"  id="themeStyle"><link rel="stylesheet" type="text/css" href="../../dark.css" disabled ><link rel="stylesheet" type="text/css" href="../../ayu.css" disabled ><script id="default-settings" ></script><script src="../../storage.js"></script><script src="../../crates.js"></script><noscript><link rel="stylesheet" href="../../noscript.css"></noscript><link rel="alternate icon" type="image/png" href="../../favicon-16x16.png"><link rel="alternate icon" type="image/png" href="../../favicon-32x32.png"><link rel="icon" type="image/svg+xml" href="../../favicon.svg"><style type="text/css">#crate-search{background-image:url("../../down-arrow.svg");}</style></head><body class="rustdoc mod"><!--[if lte IE 11]><div class="warning">This old browser is unsupported and will most likely display funky things.</div><![endif]--><nav class="sidebar"><div class="sidebar-menu" role="button">&#9776;</div><a href='../../tock_registers/index.html'><div class='logo-container rust-logo'><img src='../../rust-logo.png' alt='logo'></div></a><h2 class="location">Module interfaces</h2><div class="sidebar-elems"><div class="block items"><ul><li><a href="#traits">Traits</a></li></ul></div><div id="sidebar-vars" data-name="interfaces" data-ty="mod" data-relpath="./"></div><script defer src="./sidebar-items.js"></script></div></nav><div class="theme-picker"><button id="theme-picker" aria-label="Pick another theme!" aria-haspopup="menu" title="themes"><img width="18" height="18" alt="Pick another theme!" src="../../brush.svg"></button><div id="theme-choices" role="menu"></div></div><nav class="sub"><form class="search-form"><div class="search-container"><div><select id="crate-search"><option value="All crates">All crates</option></select><input class="search-input" name="search" disabled autocomplete="off" spellcheck="false" placeholder="Click or press ‘S’ to search, ‘?’ for more options…" type="search"></div><button type="button" id="help-button" title="help">?</button><a id="settings-menu" href="../../settings.html" title="settings"><img width="18" height="18" alt="Change settings" src="../../wheel.svg"></a></div></form></nav><section id="main" class="content"><h1 class="fqn"><span class="in-band">Module <a href="../index.html">tock_registers</a>::<wbr><a class="mod" href="#">interfaces</a><button id="copy-path" onclick="copy_path(this)" title="Copy item path to clipboard"><img src="../../clipboard.svg" width="19" height="18" alt="Copy item path"></button></span><span class="out-of-band"><span id="render-detail"><a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">[<span class="inner">&#x2212;</span>]</a></span><a class="srclink" href="../../src/tock_registers/interfaces.rs.html#1-273" title="goto source code">[src]</a></span></h1><details class="rustdoc-toggle top-doc" open><summary class="hideme"><span>Expand description</span></summary><div class="docblock"><p>Interfaces (traits) to register types</p>
<p>This module contains traits which reflect standardized interfaces
to different types of registers. Examples of registers
implementing these interfaces are <a href="../registers/struct.ReadWrite.html"><code>ReadWrite</code></a> or
<a href="../registers/struct.InMemoryRegister.html"><code>InMemoryRegister</code></a>.</p>
<p>Each trait has two associated type parameters, namely:</p>
<ul>
<li>
<p><code>T</code>: <a href="../trait.UIntLike.html"><code>UIntLike</code></a>, indicating the underlying
integer type used to represent the register’s raw contents.</p>
</li>
<li>
<p><code>R</code>: <a href="../trait.RegisterLongName.html"><code>RegisterLongName</code></a>, functioning
as a type to identify this register’s descriptive name and
semantic meaning. It is further used to impose type constraints
on values passed through the API, such as
<a href="../fields/struct.FieldValue.html"><code>FieldValue</code></a>.</p>
</li>
</ul>
<p>Registers can have different access levels, which are mapped to
different traits respectively:</p>
<ul>
<li>
<p><a href="trait.Readable.html" title="Readable"><code>Readable</code></a>: indicates that the current value of this register
can be read. Implementations will need to provide the
<a href="trait.Readable.html#tymethod.get"><code>get</code></a> method.</p>
</li>
<li>
<p><a href="trait.Writeable.html" title="Writeable"><code>Writeable</code></a>: indicates that the value of this register can be
set. Implementations will need to provide the
<a href="trait.Writeable.html#tymethod.set"><code>set</code></a> method.</p>
</li>
<li>
<p><a href="trait.ReadWriteable.html" title="ReadWriteable"><code>ReadWriteable</code></a>: indicates that this register can be
<em>modified</em>. It is not sufficient for registers to be both read-
and writable, they must also have the same semantic meaning when
read from and written to. This is not true in general, for
example a memory-mapped UART register might transmit when
writing and receive when reading.</p>
<p>If a type implements both <a href="trait.Readable.html" title="Readable"><code>Readable</code></a> and <a href="trait.Writeable.html" title="Writeable"><code>Writeable</code></a>, and
the associated <a href="../trait.RegisterLongName.html"><code>RegisterLongName</code></a>
type parameters are identical, it will automatically implement
<a href="trait.ReadWriteable.html" title="ReadWriteable"><code>ReadWriteable</code></a>. In particular, for
<a href="../registers/struct.Aliased.html"><code>Aliased</code></a> this is – in general –
not the case, so</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="macro">register_bitfields!</span>[<span class="ident">u8</span>,
    <span class="ident">A</span> [
        <span class="ident">DUMMY</span> <span class="ident">OFFSET</span>(<span class="number">0</span>) <span class="ident">NUMBITS</span>(<span class="number">1</span>) [],
    ],
];
<span class="kw">let</span> <span class="ident">read_write_reg</span>: <span class="kw-2">&amp;</span><span class="ident">ReadWrite</span><span class="op">&lt;</span><span class="ident">u8</span>, <span class="ident">A::Register</span><span class="op">&gt;</span> <span class="op">=</span> <span class="kw">unsafe</span> {
    <span class="ident">core::mem::transmute</span>(<span class="ident">Box::leak</span>(<span class="ident">Box::new</span>(<span class="number">0_u8</span>)))
};
<span class="ident">ReadWriteable::modify</span>(<span class="ident">read_write_reg</span>, <span class="ident">A::DUMMY::SET</span>);</code></pre></div>
<p>works, but not</p>

<div class='information'><div class='tooltip compile_fail'>ⓘ</div></div><div class="example-wrap"><pre class="rust rust-example-rendered compile_fail"><code><span class="macro">register_bitfields!</span>[<span class="ident">u8</span>,
    <span class="ident">A</span> [
        <span class="ident">DUMMY</span> <span class="ident">OFFSET</span>(<span class="number">0</span>) <span class="ident">NUMBITS</span>(<span class="number">1</span>) [],
    ],
    <span class="ident">B</span> [
        <span class="ident">DUMMY</span> <span class="ident">OFFSET</span>(<span class="number">0</span>) <span class="ident">NUMBITS</span>(<span class="number">1</span>) [],
    ],
];
<span class="kw">let</span> <span class="ident">aliased_reg</span>: <span class="kw-2">&amp;</span><span class="ident">Aliased</span><span class="op">&lt;</span><span class="ident">u8</span>, <span class="ident">A::Register</span>, <span class="ident">B::Register</span><span class="op">&gt;</span> <span class="op">=</span> <span class="kw">unsafe</span> {
    <span class="ident">core::mem::transmute</span>(<span class="ident">Box::leak</span>(<span class="ident">Box::new</span>(<span class="number">0_u8</span>)))
};
<span class="ident">ReadWriteable::modify</span>(<span class="ident">aliased_reg</span>, <span class="ident">A::DUMMY::SET</span>);</code></pre></div>
</li>
</ul>
<h2 id="example-implementing-a-custom-register-type" class="section-header"><a href="#example-implementing-a-custom-register-type">Example: implementing a custom register type</a></h2>
<p>These traits can be used to implement custom register types, which
are compatible to the ones shipped in this crate. For example, to
define a register which sets a <code>u8</code> value using a Cell reference,
always reads the bitwise-negated vale and prints every written
value to the console:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">struct</span> <span class="ident">DummyRegister</span><span class="op">&lt;</span><span class="lifetime">&#39;a</span>, <span class="ident">R</span>: <span class="ident">RegisterLongName</span><span class="op">&gt;</span> {
    <span class="ident">cell_ref</span>: <span class="kw-2">&amp;</span><span class="lifetime">&#39;a</span> <span class="ident">Cell</span><span class="op">&lt;</span><span class="ident">u8</span><span class="op">&gt;</span>,
    <span class="ident">_register_long_name</span>: <span class="ident">PhantomData</span><span class="op">&lt;</span><span class="ident">R</span><span class="op">&gt;</span>,
}

<span class="kw">impl</span><span class="op">&lt;</span><span class="lifetime">&#39;a</span>, <span class="ident">R</span>: <span class="ident">RegisterLongName</span><span class="op">&gt;</span> <span class="ident">Readable</span> <span class="kw">for</span> <span class="ident">DummyRegister</span><span class="op">&lt;</span><span class="lifetime">&#39;a</span>, <span class="ident">R</span><span class="op">&gt;</span> {
    <span class="kw">type</span> <span class="ident">T</span> <span class="op">=</span> <span class="ident">u8</span>;
    <span class="kw">type</span> <span class="ident">R</span> <span class="op">=</span> <span class="ident">R</span>;

    <span class="kw">fn</span> <span class="ident">get</span>(<span class="kw-2">&amp;</span><span class="self">self</span>) <span class="op">-</span><span class="op">&gt;</span> <span class="ident">u8</span> {
        <span class="comment">// Return the bitwise-inverse of the current value</span>
        <span class="op">!</span><span class="self">self</span>.<span class="ident">cell_ref</span>.<span class="ident">get</span>()
    }
}

<span class="kw">impl</span><span class="op">&lt;</span><span class="lifetime">&#39;a</span>, <span class="ident">R</span>: <span class="ident">RegisterLongName</span><span class="op">&gt;</span> <span class="ident">Writeable</span> <span class="kw">for</span> <span class="ident">DummyRegister</span><span class="op">&lt;</span><span class="lifetime">&#39;a</span>, <span class="ident">R</span><span class="op">&gt;</span> {
    <span class="kw">type</span> <span class="ident">T</span> <span class="op">=</span> <span class="ident">u8</span>;
    <span class="kw">type</span> <span class="ident">R</span> <span class="op">=</span> <span class="ident">R</span>;

    <span class="kw">fn</span> <span class="ident">set</span>(<span class="kw-2">&amp;</span><span class="self">self</span>, <span class="ident">value</span>: <span class="ident">u8</span>) {
        <span class="macro">println!</span>(<span class="string">&quot;Setting Cell to {:02x?}!&quot;</span>, <span class="ident">value</span>);
        <span class="self">self</span>.<span class="ident">cell_ref</span>.<span class="ident">set</span>(<span class="ident">value</span>);
    }
}

<span class="macro">register_bitfields!</span>[<span class="ident">u8</span>,
    <span class="ident">DummyReg</span> [
        <span class="ident">HIGH</span> <span class="ident">OFFSET</span>(<span class="number">4</span>) <span class="ident">NUMBITS</span>(<span class="number">4</span>) [
            <span class="ident">A</span> <span class="op">=</span> <span class="number">0b0001</span>,
            <span class="ident">B</span> <span class="op">=</span> <span class="number">0b0010</span>,
            <span class="ident">C</span> <span class="op">=</span> <span class="number">0b0100</span>,
            <span class="ident">D</span> <span class="op">=</span> <span class="number">0b1000</span>,
        ],
        <span class="ident">LOW</span> <span class="ident">OFFSET</span>(<span class="number">0</span>) <span class="ident">NUMBITS</span>(<span class="number">4</span>) [],
    ],
];

<span class="comment">// Create a new DummyRegister over some Cell&lt;u8&gt;</span>
<span class="kw">let</span> <span class="ident">cell</span> <span class="op">=</span> <span class="ident">Cell::new</span>(<span class="number">0</span>);
<span class="kw">let</span> <span class="ident">dummy</span> <span class="op">=</span> <span class="ident">DummyRegister</span> {
    <span class="ident">cell_ref</span>: <span class="kw-2">&amp;</span><span class="ident">cell</span>,
    <span class="ident">_register_long_name</span>: <span class="ident">PhantomData</span>,
};

<span class="comment">// Set a value and read it back. This demonstrates the raw getters</span>
<span class="comment">// and setters of Writeable and Readable</span>
<span class="ident">dummy</span>.<span class="ident">set</span>(<span class="number">0xFA</span>);
<span class="macro">assert!</span>(<span class="ident">dummy</span>.<span class="ident">get</span>() <span class="op">=</span><span class="op">=</span> <span class="number">0x05</span>);

<span class="comment">// Use some of the automatically derived APIs, such as</span>
<span class="comment">// ReadWriteable::modify and Readable::read</span>
<span class="ident">dummy</span>.<span class="ident">modify</span>(<span class="ident">DummyReg::HIGH::C</span>);
<span class="macro">assert!</span>(<span class="ident">dummy</span>.<span class="ident">read</span>(<span class="ident">DummyReg::HIGH</span>) <span class="op">=</span><span class="op">=</span> <span class="number">0xb</span>);</code></pre></div>
</div></details><h2 id="traits" class="section-header"><a href="#traits">Traits</a></h2>
<div class="item-table"><div class="item-left module-item"><a class="trait" href="trait.ReadWriteable.html" title="tock_registers::interfaces::ReadWriteable trait">ReadWriteable</a></div><div class="item-right docblock-short"><p><a href="trait.Readable.html" title="Readable"><code>Readable</code></a> and <a href="trait.Writeable.html" title="Writeable"><code>Writeable</code></a> register, over the same
<a href="../trait.RegisterLongName.html" title="RegisterLongName"><code>RegisterLongName</code></a></p>
</div><div class="item-left module-item"><a class="trait" href="trait.Readable.html" title="tock_registers::interfaces::Readable trait">Readable</a></div><div class="item-right docblock-short"><p>Readable register</p>
</div><div class="item-left module-item"><a class="trait" href="trait.Writeable.html" title="tock_registers::interfaces::Writeable trait">Writeable</a></div><div class="item-right docblock-short"><p>Writeable register</p>
</div></div></section><section id="search" class="content hidden"></section><div id="rustdoc-vars" data-root-path="../../" data-current-crate="tock_registers" data-search-index-js="../../search-index.js" data-search-js="../../search.js"></div>
    <script src="../../main.js"></script>
</body></html>